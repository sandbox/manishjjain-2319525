<?php

/**
 * Build the banner image's admin settings form
 *
 * @return a form array
 */
function banner_image_admin_settings_form($form, &$form_state) {
    $form['banner_image_default_image'] = array(
        '#type' => 'textfield',
        '#title' => t('Image path'),
        '#default_value' => variable_get('banner_image_default_image', 'public://'),
        '#description' => t('A Drupal path to the image to use as a default.'),
        '#required' => FALSE,
    );
    $form['banner_image']['image_upload'] = array(
        '#type' => 'file',
        '#title' => t('Upload a new banner image'),
        '#maxlength' => 40,
        '#description' => t("If you don't have direct file access to the server, use this field to upload your icon."),
    );

    //checking for all the available image styles.
    $options = array();
    foreach (image_styles() as $pid => $preset) {
        $options[$preset['name']] = $preset['name'];
    }
    //this is not actuaaly a style, but just an option used to bypass all the image styles and display the image in its original size
    $options['original'] = 'original';

    if (!empty($options)) {
        $form['banner_image_style_default'] = array(
            '#type' => 'select',
            '#title' => t('Image default style'),
            '#default_value' => variable_get('banner_image_style_default', 'original'),
            '#description' => t('Choose a default !link to be used for banner image. This setting can be overwritten per menu item.', array('!link' => l(t('Image style'), 'admin/config/media/image-styles'))),
            '#required' => FALSE,
            '#options' => $options,
        );
    }

    $form['banner_image_folder'] = array(
        '#type' => 'textfield',
        '#title' => t('Image folder'),
        '#default_value' => variable_get('banner_image_folder', 'banner_image'),
        '#description' => t('The name of the files directory in which the new uploaded images will be stored. This folder will be created in the files directory'),
        '#required' => FALSE,
    );

    $form['actions'] = array(
        '#tree' => FALSE,
        '#type' => 'actions',
    );
    $form['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save settings'),
    );

    return $form;
}

function banner_image_admin_settings_form_submit($form, &$form_state) {
    $directory_path = banner_image_directory_path();
    file_prepare_directory($directory_path, FILE_CREATE_DIRECTORY);

    // Store the current banner image variables
    $path = $form_state['values']['banner_image_default_image'];
    $image_style = $form_state['values']['banner_image_style_default'];

    // Define the validation settings
    $validate = array(
        'file_validate_is_image' => array(),
    );

    // Check for a new uploaded banner image, and use that instead.
    if ($file = file_save_upload('image_upload', $validate)) {
        $parts = pathinfo($file->filename);
        $filename = $directory_path . '/banner_image_' . $parts['extension'];
        //create a new file if he image does not exist or replace the existing one with the current file.
        file_unmanaged_copy($file->uri, $filename, FILE_EXISTS_REPLACE);

        // Flush image style generated images
        image_path_flush($filename);

        $path = $filename;
        variable_set('banner_image_default_image', $filename);
    }
}
