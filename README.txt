<STRONG>INTRODUCTION</STRONG>
----------------------------------------------------------------------------------------------------------------------------------

This module is use to display banner images. A banner image block is provided by this module and can be set in any desired region. 
The images are set using the menu-edit pages, if not set the default Banner Image will be used. The user can do the follow things:

1) Provide a path to an already existing image or upload a new image itself.
2) Provide a path to which the Image should link to (optional).
3) Set the directory where the image should be stored.
4) Finally, can also set image styles for the Banner Images Individually and as a whole.


<STRONG>INSTALLATION & CONFIGURATION</STRONG>
-----------------------------------------------------------------------------------------------------------------------------------

1) Install module and enable it.
2) Go to the module admin <strong><a href="/admin/config/user-interface/banner-image">Configuration Page</a></strong> and set the default settings.
3) Finally, go to the <strong><a href="/admin/structure/block">Blocks</a></strong> page and set the 'Banner Image' block in the desired region.

<STRONG>USAGE</STRONG>
-----------------------------------------------------------------------------------------------------------------------------------

This module provides a fieldset in the menu-edit form using the <i>hook_form_FORMID_alter()</i>, the values entered in this fieldset 
are stored in the options column in the menu_links column using the <i>hook_form_submit()</i>.
A block is then created using <i>hook_block_info(), hook_block_configure(), hook_block_save()</i> and <i>hook_block_view().</i> This block then display the 
image stored previously in the menu-edit form. It also provides a admin settings form where admins can set the default Banner Images.
Other hooks implemented by this module includes

1) <i>hook_menu()</i> to create the admin settings page.
2) <i>hook_help()</i> to create the help page you are reading.
3) <i>hook_permission()</i> to provide permission on the usage of the module.
4) <i>hook_install()</i> to install the modules necessary variables.
5) <i>hook_enable()</i> to display the module enabled message.
6) <i>hook_uninstall()</i> to safely uninstall the module leaving no traces of its existence.
 
<STRONG>CONTACT</STRONG>
-----------------------------------------------------------------------------------------------------------------------------------

This module was written by, and is maintained by,
Heather Ohana <<i>heather.ohana@nike.com</i>>
Manish Jain <<i>manish.jain4@nike.com</i>>
